#!/usr/bin/env python

## 
# Estimulador Visual SSVEP, de un sola frecuencia.
# Copyright (C) 2018 Signal Processing and Recognition Group.
## 

# ---------------------------
# Importacion de los modulos
# ---------------------------
import pygame   
import argparse 
import sys
import time as tm
import ctypes


class Window(object):
    global inicio, Relajacion
    user32 = ctypes.windll.user32
    user32.SetProcessDPIAware()
    ancho, alto = user32.GetSystemMetrics(0), user32.GetSystemMetrics(1)
    # ancho = 500
    # alto = 400
    width = ancho     # Ancho de la ventana.
    height = alto    # Alto de la ventana.

    # Argumentos de entrada.
    parser = argparse.ArgumentParser('Stimulator of SSVEP')
    parser.add_argument("frequency", type=float, help = "frecuency of stimulus. (default: 20.0 Hz)", nargs = "?")
    parser.add_argument("color", type=float, help = "color. (default: blue)", nargs = "?")
    args = parser.parse_args()
    Freq = args.frequency
    colorselec = args.color
    fps = 60             # frecuencia de la ventana.
    Relajacion = 10       # Segundos de relajacion.
    if len(sys.argv) == 2:
        stimulus_freq = Freq # Frecuencia de Estimulo.
    else:
        stimulus_freq = 20 
    run_times = int(round(fps/stimulus_freq))

    pygame.init()
    pygame.display.set_caption("Ssvep stimulus v_1.4.0")
    screen = pygame.display.set_mode((width,height),0,32)
            
    #Colores de Estimulo. 
    if colorselec == 1:
    #background_color = [[0, 0, 0], [255,255,255]]      # Negro - Blanco
        background_color = [[0, 0, 0], [255,102,102]]      # Negro - Rojo
    elif colorselec == 2:
        background_color = [[0, 0, 0], [255,178,102]]      # Negro - Naranja
    #background_color = [[0, 0, 0], [255, 255, 102]]    # Negro - Amarillo
    elif colorselec == 3:
        background_color = [[0, 0, 0], [178, 255, 102]]    # Negro - Verde Claro
    #background_color = [[0, 0, 0], [178, 255, 102]]    # Negro - Verde Oscuro
    #background_color = [[0, 0, 0], [102, 255, 178]]    # Negro - Verde aguamarina
    #background_color = [[0, 0, 0], [102,255,255]]      # Negro - azul claro
    else:
        background_color = [[0, 0, 0], [102,102,255]]       # Negro - azul 
    background_color1 = [[0, 0, 0], [0,0,0]]            # Fondo Negro.
    background = pygame.Surface(screen.get_size())      # superficie.
    background = background.convert()
    background.fill((0, 0, 0))                          # color inicial de la superficie.

    clock = pygame.time.Clock()
    all_time = 0.0
    
    #Variables to switch  background_color position
    cur_len = 0
    a = -1
    font = pygame.font.SysFont('Console', 33)
    inicio = tm.time()
    
    def run(self):
        final = tm.time()
        run_count = 1
        hecho = False
        while not hecho:
            for event in pygame.event.get(): 
                if event.type == pygame.QUIT:                               # Si el usuario hizo click sobre salir
                    hecho = True                                            # Marcamos que hemos acabado y abandonamos este bucle
                if event.type == pygame.KEYDOWN:
                    if (event.unicode == u'q') or (event.unicode == u'Q'):  # condicion de salida del bucle con la tecla q o Q.
                        hecho = True                
            # if self.all_time >= 60:
            #         hecho = True
            milliseconds = self.clock.tick(self.fps)                        # By calling Clock.tick(fps) once per frame, the program will never run at more than fps frames per second.

            self.all_time += milliseconds / 1000.0
            # print int(self.all_time)
            #text-------------
            text = "+"
            #text = "+ %s sec" %(str(self.all_time))
            font_width, font_height = self.font.size(text)
            text_plus = self.font.render(text, 1,  (0,204,204))
            self.screen.blit(text_plus, ((self.width - font_width) // 2, (self.height - font_height) // 2))
            #-----------------

            #switch background color-----------------
            if self.run_times == run_count:
                run_count = 1

                #switch  background_color position
                self.a = self.a*-1
                self.cur_len = self.cur_len + self.a
                # print int(final-inicio)
                if self.all_time < Relajacion:
                    self.background.fill(self.background_color1[self.cur_len])
                else:
                    self.background.fill(self.background_color[self.cur_len])
            else:
                run_count += 1
            #-----------------

            pygame.display.flip()
            self.screen.blit(self.background, (0, 0))
            
        pygame.quit()

# inicio.
if __name__ == '__main__':
    Window().run()   