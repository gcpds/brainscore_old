# coding=utf-8

## @package GUI_v3.py - Grupo GCPDS
#  Copyright (C) 2018 Signal Processing and Recognition Group
#  Implementación del módulo "open_bci_GCPDS" para la conexión con la tarjeta openBci, junto con la visualización
#  de la clasificación de los estímulos.
#  @author Grupo GCPDS.
#  @version 3.2.0

"""
Core OpenBCI object for handling connections and samples from the board.

EXAMPLE USE:

def handle_sample(sample):
  print(sample.channel_data)

board = OpenBCIBoard()
board.print_register_settings()
board.start_streaming(handle_sample)

NOTE: If daisy modules is enabled, the callback will occur every two samples, hence "packet_id" will only contain even numbers. As a side effect, the sampling rate will be divided by 2.

FIXME: at the moment we can just force daisy mode, do not check that the module is detected.
TODO: enable impedance

"""
import scipy
import sys
sys.path.append('..')                       # Ayuda a encontrar el script open_bci_GCPDS
import open_bci_GCPDS as bci                            # Módulo con las funciones para la conexión y la adquisición de datos de la tarjeta
import os                                               # ---.
import sys                                              # ---.
import logging                                          # ---.
import time as tm                                       # Módulo - Time.
from datetime import datetime, date                     # ---.
import numpy as np                                      # Módulo para realizar calculos.
from numpy.matlib import repmat                         # .
from scipy.signal import lfilter                        # Módulo para realizar el filtrado.
from scipy.signal import filtfilt, parzen               # Módulo para realizar el filtrado.
from scipy.stats import mode                            # ---.
from scipy.interpolate import interp1d                  # Módulo para interpolar.
from sklearn.neighbors import KNeighborsClassifier      # Módulo para clasificar.
import cPickle                                          # ---.
import pyedflib                                         # Módulo para guardar base de datos en formato EDF/EDF+/BDF/BDF+.
import matplotlib.pyplot as plt                         # .
from scipy.fftpack import fft                           # Módulo para calcular el espectro de la señal por medio de la FFT.
from scipy.signal import welch                          # Módulo para calcular el espectro de la señal por medio de la welch.
import pyqtgraph as pg                                  # Módulo para generar la ventana principal de graficación.
from pyqtgraph.Qt import QtGui, QtCore                  # Módulo para usar la ventana de graficación.
from pyqtgraph.ptime import time                        # ---.
# import pygame                                           # Módulo para realizar ventana interactiva.
import operator                                         # Módulo para realizar modificaciones en la graficación de varias señales en una misma grafica (legends).
# import subprocess
from statistics import mode
import OSC


OSCaddr = '192.168.0.110'


def execute_for_some_time(F1, F2, F3, F4, Edelta, Hdelta, Etheta, Htheta, EAlpha, HAlpha, EBeta, HBeta):
    c = OSC.OSCClient()
    c.connect((OSCaddr, 2020))  # connect to Max 
    oscmsg = OSC.OSCMessage()
    oscmsg.setAddress("/BrainRhythms")
    oscmsg.append(str(F1))
    oscmsg.append(str(F2))
    oscmsg.append(str(F3))
    oscmsg.append(str(F4))
    oscmsg.append(str(Edelta))
    oscmsg.append(str(Hdelta))
    oscmsg.append(str(Etheta))
    oscmsg.append(str(Htheta))
    oscmsg.append(str(EAlpha))
    oscmsg.append(str(HAlpha))
    oscmsg.append(str(EBeta))
    oscmsg.append(str(HBeta))
    c.send(oscmsg)
    # send_osc(dateTime)


##  Llamado para recivir los datos.
#   Es el llamado necesario en "start_streaming" del módulo "open_bci_GCPDS" para realizar el manejo de los datos
#   que llegan de la tarjeta, en este caso los agrega en la variable eeg.
#   @param  . cuando se llama de la manera "board.start_streaming(saveData)" no requiere parametros .
##  @retval . No devuelve nada. pero sí agrega los valores de los canales a la variable eeg.
def saveData(sample):
    global eeg
    eeg.append(sample.channel_data)

##  Conexión con la tarjeta openBci
#   Crea el objeto board mediante "OpenBCIBoard" del módulo "open_bci_GCPDS" para manejar el envío de datos de la tarjeta.
#   @param  . No requiere parámetros.
##  @retval . Devuelve el objeto "Board" con las propiedades de la clase "openBCIBoard".


def connect_board():
    baud = 115200
    board = bci.OpenBCIBoard(port=None, baud=baud, filter_data=True)
    print("Board Connected")
    return board

##  Inizializa la tarjeta
#   Le dice a la tarjeta que inicie el envío de datos, activa los filtros de la misma, define las variables para los filtros
#   predeterminados que son: Notch en 60 hz, pasa bandas orden 3 Butterworth de 5 - 45 hz.
#   @param  . Requiere el objeto board entregado por "connect_board".
##  @retval . No devuelve nada.


def initialize(board):
    global b, a, eeg, b_n, a_n, b1, a1, filter_used, filter_used1, filter_notch_used, eeg_to_save
    eeg = []
    if board:
        board.ser.write('v')
    tm.sleep(1)

    if board:
        board.enable_filters()
    b_n = [1.0000, -0.1297, 1.0000]    # notch: 60 hz.
    a_n = [1.0000, -0.1032, 0.5914]
    b = [0.0579, 0, -0.1737, 0, 0.1737, 0, -0.0579]      # 5 - 45Hz butter filter
    a = [1.0000, -3.7335, 5.9137, -5.2755, 2.8827, -0.9042, 0.1180]
    filter_used = 'PB: 5-45Hz'
    b1 = [0.00177794, 0, -0.00888972, 0, 0.01777944, 0, -0.01777944, 0, 0.00888972, 0, -0.00177794]      # 3 - 30 Hz butter filter - orden 5
    a1 = [1, -7.584859, 26.10585504, -53.79783728, 73.62002475, -69.97060817, 46.79479349, -21.74549116, 6.71922317, -1.24649281, 0.10539219]
    filter_used1 = 'PB: 3-30Hz'

    filter_notch_used = 'N: 60Hz'
    tm.sleep(0.1)

    if board:
        board.start_streaming(saveData)
    print('Board initializated')

##  Se desconecta de la tarjeta.
#   Graba una última muestra pero no la guarda, procede directamente a desconectarse de la tarjeta.
#   @param  . Requiere el objeto board entregado por "connect_board".
##  @retval . No devuelve nada.


def disconnect_board(board):
    global eeg
    eeg = []
    board.ser.write('v')
    tm.sleep(0.1)
    board.start_streaming(saveData)
    print('Streaming ended')
    print('')
    board.disconnect()
    sys.exit()

##  Pre proceso 1 del EEG.
#   Como los datos de la tarjeta son crudos, se escalan adecuadamente para que estén en uVoltios.
#   Luego procede a centrarlos en la media y realizar el filtrado segun [b,a] --> pasabandas, y [b_n,a_n] --> notch.
#   Los datos crudos se escalan multiplicando por 2.23517444553071e-08.
#   @param  . Requiere la lista "eeg", que es entregada por "get_n_secs" (EEG desde la tarjeta, cada canal en una columna).
##  @retval . Devuelve "eeg_processed" que tiene las mismas dimensiones de la matriz "eeg" de entrada, pero está transpuesta.


def pre_process(eeg):
    eeg = np.array(eeg)
    [fil, col] = eeg.shape
    eeg_processed = np.zeros([fil, col])
    for i in range(fil):
        data = eeg[i, :] * 2.23517444553071e-08  # 2.23517444553071e-08 factor de escala en uV
        data = data - np.mean(data)
        data = lfilter(b_n, a_n, data)
        data = lfilter(b, a, data)
        eeg_processed[i, :] = data
    return(eeg_processed)

##  Pre proceso 2 del EEG.
#   Como los datos de la tarjeta son crudos, se escalan adecuadamente para que estén en uVoltios.
#   Luego procede a centrarlos en la media y realizar el filtrado segun [b,a] --> pasabandas, y [b_n,a_n] --> notch.
#   Los datos crudos se escalan multiplicando por 2.23517444553071e-08.
#   @param  . Requiere la lista "eeg", que es entregada por "get_n_secs" (EEG desde la tarjeta, cada canal en una columna).
##  @retval . Devuelve "eeg_processed" que tiene las mismas dimensiones de la matriz "eeg" de entrada, pero está transpuesta.


def pre_process2(eeg):
    eeg = np.array(eeg)
    [fil, col] = eeg.shape
    eeg_processed = np.zeros([fil, col])
    for i in range(fil):
        data = eeg[i, :] * 2.23517444553071e-08  # 2.23517444553071e-08 factor de escala en uV
        data = data - np.mean(data)
        data = lfilter(b_n, a_n, data)
        data = lfilter(b1, a1, data)
        data = data - np.mean(data)
        eeg_processed[i, :] = data
    return(eeg_processed)

##  Pre proceso del EEG.
#   Como los datos de la tarjeta son crudos, se escalan adecuadamente para que estén en uVoltios.
#   Luego procede a centrarlos en la media y realizar el filtrado segun [b,a] --> pasabandas, y [b_n,a_n] --> notch.
#   Los datos crudos se escalan multiplicando por 2.23517444553071e-08.
#   Organiza los datos que permiten visualizar cada uno de los canales.
#   @param  . Requiere la lista "eeg", que es entregada por "get_n_secs" (EEG desde la tarjeta, cada canal en una columna).
##  @retval . Devuelve "eeg_processed" que tiene las mismas dimensiones de la matriz "eeg" de entrada, pero está transpuesta.


def pre_process_plot(eeg):
    eeg = np.array(eeg)
    [fil, col] = eeg.shape
    eeg_processed = np.zeros([fil, col])
    for i in range(fil):
        data = eeg[i, :] * 2.23517444553071e-08  # 2.23517444553071e-08 factor de escala en uV
        data = data - np.mean(data)
        data = lfilter(b_n, a_n, data)
        data = lfilter(b, a, data)
        data = (data * 100000) + ((i + 1) * 100)
        eeg_processed[i, :] = data
    return(eeg_processed)

##  Pre proceso de CCA del EEG.
#   Como los datos de la tarjeta son crudos, se escalan adecuadamente para que estén en uVoltios.
#   Luego procede a centrarlos en la media y realizar el filtrado segun [b,a] --> pasabandas, y [b_n,a_n] --> notch.
#   Los datos crudos se escalan multiplicando por 2.23517444553071e-08.
#   @param  . Requiere la lista "eeg", que es entregada por "get_n_secs" (EEG desde la tarjeta, cada canal en una columna).
##  @retval . Devuelve "eeg_processed" que tiene las mismas dimensiones de la matriz "eeg" de entrada, pero está transpuesta.


def pre_process_cca(eeg):
    eeg = np.array(eeg)
    [fil, col] = eeg.shape
    eeg_processed = np.zeros([fil, col])
    for i in range(fil):
        data = eeg[i, :] * 2.23517444553071e-08  # 2.23517444553071e-08 factor de escala en uV
        data = data - np.mean(data)
        data = data / np.std(data)
        data = lfilter(b_n, a_n, data)
        data = lfilter(b, a, data)
        eeg_processed[i, :] = data
    return(eeg_processed)

##  Obtiene "n" segundos de datos de la tarjeta openBci.
#   Llama a la función "start_streaming" del módulo "open_bci_GCPDS" que entrega un paquete de datos, hasta que se cumpla
#   el número de muestras correspondientes a "n" segundos (la frecuencia de muestreo es 250).
#   @param  . Como parametros requiere el objeto board entregado por "connect_board" y la cantidad de segundos "n" que se desea ontener.
##  @retval . Devuelve una matriz de el número de datos en el tiempo "n" por canales.


def get_n_secs(board, n):
    global eeg
    eeg = []

    for i in range(int(round(n * 250))):
        board.start_streaming(saveData)

    return(eeg)

#def get_n_secs(board=None, n=None):
    #global eeg
    #t = np.linspace(0, n, int(round(n * 250)))
    #f = 20
    #eeg = np.array([np.sin(2 * np.pi * f * t) for i in range(8)]) + np.random.uniform(size=(8, int(round(n * 250))))

    #return(eeg.T)

##  Obtiene las caracteristicas de la señal EEG.
#   Calcula la señal.
#   @param  . Como parametros requiere el objeto board entregado por "connect_board" y la cantidad de segundos "n" que se desea ontener.
##  @retval . Devuelve una matriz de el número de datos en el tiempo "n" por canales.


def getFeatures(x):
    F = np.zeros((1, 3))

    # - EnergyF[:,1] = np.mean(np.std(x,axis=1))
    F[:, 0] = np.mean(np.sum(abs(x ** 2), axis=1))

    # - Standard deviation
    F[:, 1] = np.mean(np.std(x, axis=1))

    # - Entropy
    m, n = x.shape
    H = np.zeros(m)
    for i in range(m):
        upper = np.mean(x[i, :]) + 3 * np.std(x[i, :])
        lower = np.mean(x[i, :]) - 3 * np.std(x[i, :])
        bins = np.arange(lower, upper, step=20)
        bin_widths = bins[1:] - bins[0:-1]
        counts, bins = np.histogram(x[i, :], bins)
        p = counts / np.sum(counts)
        H[i] = -np.sum(np.compress(p != 0, p * (np.log(p) - np.log(bin_widths))))
    F[:, 2] = np.mean(H)
    return F

##  Obtiene la entropia.
#   Con la señal EEG Calcula la entropia de la señal adquirida de los canales.
#   @param  . Como parametros requiere el objeto board entregado por "connect_board" y la cantidad de segundos "n" que se desea ontener.
##  @retval . Devuelve una matriz de el número de datos en el tiempo "n" por canales.


def getEntropy(x):
    # - Entropy
    m, n = x.shape
    H = np.zeros(m)
    for i in range(m):
        upper = np.mean(x[i, :]) + 3 * np.std(x[i, :])
        lower = np.mean(x[i, :]) - 3 * np.std(x[i, :])
        # bins = np.arange(lower, upper, step=20)
        bins = np.linspace(lower, upper, num=20)
        bin_widths = bins[1:] - bins[0:-1]
        counts, bins = np.histogram(x[i, :], bins)
        counts = counts * 1.0
        p = counts / np.sum(counts)
        p = np.compress(p != 0, p)
        #H[i] = -np.sum(np.compress(p != 0, p*(np.log(p) - np.log(bin_widths))))
        H[i] = -np.sum(p * (np.log(p))) / len(p)
    H_end = np.mean(H)
    return H_end

##  Obtiene la energia
#   Calcula la energia de la señal de cada uno de los canales.
#   @param  . Como parametros requiere el objeto board entregado por "connect_board" y la cantidad de segundos "n" que se desea ontener.
##  @retval . Devuelve una matriz de el número de datos en el tiempo "n" por canales.


def getEnergy(x, e_yp):
    # - Energy
    E = np.mean(np.sum(abs(x ** 2), axis=1)) / e_yp
    return E

##  Obtiene la desviacion de los canales.
#   Calcula la desviacion que se presenta en los diferentes canales.
#   @param  . Como parametros requiere el objeto board entregado por "connect_board" y la cantidad de segundos "n" que se desea ontener.
##  @retval . Devuelve una matriz de el número de datos en el tiempo "n" por canales.


def getSD(x):
    # - Standard deviation
    SD = np.mean(np.std(x, axis=1))
    return SD

##  Obtiene el espectro de la señal.
#   El uso de la FFT identifica el comportamiento de la señal EEG por medio del espectro.
#   @param  . Como parametros requiere el objeto board entregado por "connect_board" y la cantidad de segundos "n" que se desea ontener.
##  @retval . Devuelve una matriz de el número de datos en el tiempo "n" por canales.


def getSpectrumtemp(x, Ts):
    m = len(x)
    #Dejando como numero par el tamaño
    #(evitar problemas al tomar la parte positiva de la transformada)
    if (m % 2 != 0):
        x = x[0:m - 1, :]
    Y = np.fft.fft(x)
    Y = np.fft.fftshift(Y)
    Yf = Y[len(Y) / 2:]

    f = np.fft.fftfreq(len(x), Ts)
    f = np.fft.fftshift(f)
    ff = f[len(f) / 2:]

    ps = np.abs(Yf)**2
    #freqs = np.fft.fftfreq(len(x), time_step)
    #idx = np.argsort(freqs)
    #plt.plot(freqs[idx], ps[idx])
    return ps, ff

##  Obtiene el espectro de la señal.
#   El uso de la FFT identifica el comportamiento de la señal EEG por medio del espectro.
#   @param  . Como parametros requiere el objeto board entregado por "connect_board" y la cantidad de segundos "n" que se desea ontener.
##  @retval . Devuelve una matriz de el número de datos en el tiempo "n" por canales.


def getSpectrum(x, T):
    # Number of sample points
    N = len(x)
    # sample spacing
    yf = fft(x)
    p = 2.0 / N * np.abs(yf[0:N // 2])
    f = np.linspace(0.0, 1.0 / (2.0 * T), N // 2)
    return p, f

##  Obtiene el espectro de la señal.
#   El uso de la Welch identifica el compotamiento de la señal EEG por medio del espectro.
#   @param  . Como parametros requiere el objeto board entregado por "connect_board" y la cantidad de segundos "n" que se desea ontener.
##  @retval . Devuelve una matriz de el número de datos en el tiempo "n" por canales.


def getwelch(x, fs):
    f, p = welch(x, fs, 'flattop', 1024, scaling='spectrum')
    return f, p

##  Obtiene las caracteristicas de CCA
#   @param  . Como parametros requiere el objeto board entregado por "connect_board" y la cantidad de segundos "n" que se desea ontener.
##  @retval . Devuelve una matriz de el número de datos en el tiempo "n" por canales.


def cca(X, Y):
    try:
        z = np.concatenate((X, Y), axis=0)
        C = np.cov(z)
        sx = X.shape[0]
        sy = Y.shape[0]
        Cxx = C[0:sx, 0:sx] + (10e-8 * np.eye(sx))
        Cxy = C[0:sx, sx:sx + sy]
        Cyx = np.transpose(Cxy)
        Cyy = C[sx:sx + sy, sx:sx + sy] + ((10e-8) * np.eye(sy))

        Rx = np.transpose(np.linalg.cholesky(Cxx))
        invRx = np.linalg.inv(Rx)

        #Z = invRx' * Cxy * (Cyy\Cyx) * invRx
        Z = np.dot(np.transpose(invRx), Cxy)
        Op_c = np.dot(np.linalg.inv(Cyy), Cyx)
        Z = np.dot(Z, Op_c)
        Z = np.dot(Z, invRx)
        #Z = np.dot( np.dot( np.dot( np.transpose(invRx) , Cxy ) , np.dot( np.linalg.inv(Cyy) , Cyx ) ) , invRx )
        Z = 0.5 * (Z + np.transpose(Z))
        r, Wx = np.linalg.eig(Z)
        r = np.sqrt(np.real(r))
        Wx = np.dot(invRx, Wx)
        Wy = np.dot(Op_c, Wx)
        Wy = Wy / np.matlib.repmat(r, sy, 1)
        flag_c = True
    except:
        Wx = 0
        Wy = 0
        r = 0
        flag_c = False
    return Wx, Wy, r, flag_c

##  Obtiene la energía del espectro en la banda de estimulo.
#   @param  . Como parametros requiere el objeto board entregado por "connect_board" y la cantidad de segundos "n" que se desea ontener.
##  @retval . Devuelve una matriz de el número de datos en el tiempo "n" por canales.


def getenergy_spec(Freq, f, df, Frq_, fre):
    if fre < 1:
        absminn = np.abs(Frq_ - (f - df))
        minn = np.min(absminn)
        posfmin = np.where(absminn == minn)[0][0]
        absminn = np.abs(Frq_ - (f + df))
        maxn = np.min(absminn)
        posfmax = np.where(absminn == maxn)[0][0]

        Fre = np.sum(Freq[posfmin:posfmax, ]**2)
        return Fre
    else:
        posfmin = []
        posfmax = []

        absmin = np.abs(Frq_ - (f[0] - fre))
        minn = np.min(absmin)
        posfmin.append(np.where(absmin == minn)[0][0])

        absmin = np.abs(Frq_ - (f[0] - df))
        maxn = np.min(absmin)
        posfmax.append(np.where(absmin == maxn)[0][0])

        absmin = np.abs(Frq_ - (f[0] + df))
        minn = np.min(absmin)
        posfmin.append(np.where(absmin == minn)[0][0])

        absmin = np.abs(Frq_ - (f[1] - df))
        maxn = np.min(absmin)
        posfmax.append(np.where(absmin == maxn)[0][0])

        absmin = np.abs(Frq_ - (f[1] + df))
        minn = np.min(absmin)
        posfmin.append(np.where(absmin == minn)[0][0])

        absmin = np.abs(Frq_ - (f[2] - df))
        maxn = np.min(absmin)
        posfmax.append(np.where(absmin == maxn)[0][0])

        absmin = np.abs(Frq_ - (f[2] + df))
        minn = np.min(absmin)
        posfmin.append(np.where(absmin == minn)[0][0])

        absmin = np.abs(Frq_ - (f[3] - df))
        maxn = np.min(absmin)
        posfmax.append(np.where(absmin == maxn)[0][0])

        absmin = np.abs(Frq_ - (f[3] + df))
        minn = np.min(absmin)
        posfmin.append(np.where(absmin == minn)[0][0])

        absmin = np.abs(Frq_ - (f[3] + fre))
        maxn = np.min(absmin)
        posfmax.append(np.where(absmin == maxn)[0][0])

        Fre = np.sum(Freq[posfmin[0]:posfmax[0], ]**2) + np.sum(Freq[posfmin[1]:posfmax[1], ]**2) + np.sum(Freq[posfmin[2]:posfmax[2], ]**2) + np.sum(Freq[posfmin[3]:posfmax[3], ]**2) + np.sum(Freq[posfmin[4]:posfmax[4], ]**2)
        return Fre


#------------------------------------------------------------------------------------#
#----------------------------- Interfaz gráfica de usuario. -------------------------#


app = QtGui.QApplication([])

## ----------------------------------- Variables Iniciales ----------------------------------- #

#Setting variables
fs = 250                                                     # Frecuencia de muestreo.
Ts = 1.0 / fs                                                  # Tamaño del paso.
time = 0.5                                                   # Tiempo de adquisición de la targeta de 0.4 Seg.
channels = 8                                                 # Numero de canales.
bands = 5                                                    # Numero de bandas analizadas.
win_size = 4                                                 # Tamaño de la ventana.
points_number = 5                                            # Numero de puntos para cada una de las bandas, en Entropia, Energia y Desviación
df = 1                                                       # Delta de la frecuencia.
# Frecuencias de estímulos.
stimulus_freq = [8, 15, 20, 25]                      # Frecuencias en Hz.
Num_stimulus = 4                                             # Número de estimulos.
Fre_analisys = [6, stimulus_freq[3] + 2]  # Banda de análisis.
channel_analisys = 7                                         # Canal de análilsis canal Oz.
channels_analisys = 4                                        # Número de canales analizados.
Graph_lim = [stimulus_freq[0] - 2, stimulus_freq[3] + 2]     # limite de graficación.
time_final = 10
#Variables for CCA
tLen = win_size
f = stimulus_freq
#t=np.linspace(0, (tLen/fs), num=(tLen*fs))
t = np.linspace(0, (tLen), num=875)
harmonics = 6                                                # Número de armónicos utilizados.
Y = np.zeros(((harmonics + 1) * 2, len(t), len(f)))
for fr in range(0, len(f)):
    y = np.zeros(((harmonics + 1) * 2, len(t)))
    for harm in range(0, harmonics + 1):
        y[(harm) * 2, :] = np.sin(2.0 * (harm + 1) * np.pi * f[fr] * t)
        y[(harm) * 2 + 1, :] = np.cos(2.0 * (harm + 1) * np.pi * f[fr] * t)
    Y[:, :, fr] = y
## ----------------------------------- Fin Variables Iniciales ----------------------------------- #

# Inicio de la board
board = connect_board()
#board = None

initialize(board)
# Get EEG channels [Fp1,Fpz,Fp2,Fz,C3,Cz,C4,Oz]
eeg = get_n_secs(board, time)    # Adquiere la información de cada uno de los canales (electrodos).
#eeg = np.random((8 * 250))

eeg = np.asarray(eeg)
y = np.transpose(eeg.tolist())
y_f = pre_process(y)
y_plot = pre_process_plot(y)
x = np.linspace(0, time, fs * time)

# ----------------- Main Window ------------------- #
win = pg.GraphicsWindow(title="EEG Data System")
win.resize(1000, 600)                            # Tamaño de la resolución de la ventana.
win.setWindowTitle('EEG Data System')           # Ingresa el titulo de la ventana.
# Enable antialiasing for prettier plots
pg.setConfigOptions(antialias=True)

#-----------------------------------------------------------------------------------------------------#
#-----------------------------------------------------------------------------------------------------#
# --------------------------------------------- Entropy --------------------------------------------- #
plt0 = win.addPlot(title="Entropy", row=1, col=0, colspan=1)
plt0.setWindowTitle('Entropy')
plt0.setLabel('bottom', 'Time', units='sec')
#plt0.getAxis('left').setTicks([[(0, 'Delta'), (1, 'Theta'), (2, 'Alpha'), (3, 'Beta'), (4, 'Gamma')]])
#plt0.setYRange(-1, (bands)+1)
plt0.setXRange(0, points_number + 1)
curveH = []
H = np.zeros((5, points_number))
for i in range(bands):
    c = plt0.plot(range(1, points_number + 1), H[i, :])  ## setting pen=(i,3) automaticaly creates three different-colored pe
    c.setPos(0, i)
    curveH.append(c)

#-----------------------------------------------------------------------------------------------------#
#-----------------------------------------------------------------------------------------------------#
# --------------------------------------------- Energy  --------------------------------------------- #
plt1 = win.addPlot(title="Energy", row=1, col=1, colspan=1)
plt1.setWindowTitle('Energy')
plt1.setLabel('bottom', 'Time', units='sec')
#plt1.getAxis('left').setTicks([[(0, 'Delta'), (1, 'Theta'), (2, 'Alpha'), (3, 'Beta'), (4, 'Gamma')]])
#plt1.setYRange(-1, (bands)+1)
plt1.setXRange(0, points_number + 1)
curveE = []
E = np.zeros((5, points_number))
for i in range(bands):
    c = plt1.plot(range(1, points_number + 1), E[i, :])  ## setting pen=(i,3) automaticaly creates three different-colored pe
    c.setPos(0, i)
    curveE.append(c)

#-----------------------------------------------------------------------------------------------------#
#-----------------------------------------------------------------------------------------------------#
# ------------------------------------------------ SD ----------------------------------------------- #
plt2 = win.addPlot(title="Standard Deviation", row=1, col=2, colspan=1)
plt2.setWindowTitle('Standard Deviation')
plt2.setLabel('bottom', 'Time', units='sec')
#plt2.getAxis('left').setTicks([[(0, 'Delta'), (1, 'Theta'), (2, 'Alpha'), (3, 'Beta'), (4, 'Gamma')]])
#plt2.setYRange(-1, (bands)+1)
plt2.setXRange(0, points_number + 1)
curveSD = []
SD = np.zeros((5, points_number))
for i in range(bands):
    c = plt2.plot(range(1, points_number + 1), SD[i, :])  ## setting pen=(i,3) automaticaly creates three different-colored pe
    c.setPos(0, i)
    curveSD.append(c)

#-----------------------------------------------------------------------------------------------------#
#-----------------------------------------------------------------------------------------------------#
# --------------------------------------------- EEG Plot--------------------------------------------- #
plt3 = win.addPlot(title="EEG", row=2, col=0, colspan=2)
plt3.setWindowTitle('EEG Signal')
plt3.setLabel('bottom', 'Time', units='sec')
# plt3.getAxis('left').setTicks([[(100, 'Channel 1'), (200, 'Channel 2'), (300, 'Channel 3'), (400, 'Channel 4'), (500, 'Channel 5'), (600, 'Channel 6'), (700, 'Channel 7'), (800, 'Channel 8')]])
plt3.getAxis('left').setTicks([[(100, 'Fp1'), (200, 'Fpz'), (300, 'Fp2'), (400, 'Fz'), (500, 'C3'), (600, 'Cz'), (700, 'C4'), (800, 'Oz')]])
#plt3.getAxis('left').setTicks([[(100, 'Fp1'), (200, 'Fp2'), (300, 'C3'), (400, 'Cz'), (500, 'C4'), (600, 'O1'), (700, 'Oz'), (800, 'O2')]])
plt3.setYRange(0, (channels * 100) + 50)
plt3.setXRange(0, win_size - 1)
#plt3.resize(1000, 300)

curve = []
for i in range(channels):
    c = plt3.plot(x, y[i], pen=(i, 8))  ## setting pen=(i,3) automaticaly creates three different-colored pe
    c.setPos(0, i)
    curve.append(c)
counter = time

#-----------------------------------------------------------------------------------------------------#
#-----------------------------------------------------------------------------------------------------#
# --------------------------------------------- Spectrum Model 1 ------------------------------------ #
# plt4 = win.addPlot(title="Spectrum",row=2, col=2, colspan=1)
# plt4.setWindowTitle('Spectrum')
# plt4.setLabel('bottom', 'Frequency', units='Hz')
# #plt4.resize(1000, 300)
# plt4.setXRange(Graph_lim[0],Graph_lim[1])
# ps,freqs = getSpectrum(y[6],Ts)
# curveSpec = []
# c = plt4.plot(freqs, ps)  ## setting pen=(i,3) automaticaly creates three different-colored pe
# curveSpec.append(c)

#-----------------------------------------------------------------------------------------------------#
#-----------------------------------------------------------------------------------------------------#
# --------------------------------------------- Spectrum Model 2 ------------------------------------ #
plt4 = win.addPlot(title="Spectrum", row=2, col=2, colspan=1)
plt4.setWindowTitle('Spectrum')
plt4.setLabel('bottom', 'Frequency', units='Hz')
#plt4.resize(1000, 300)
plt4.setXRange(Graph_lim[0], Graph_lim[1])
plt4.setYRange(0, 1)
# print(y[channel_analisys].shape)
ps, freqs = getSpectrum(y[channel_analisys], Ts)
curveSpec = []
c = plt4.plot(freqs[1:], ps[1:])  ## setting pen=(i,3) automaticaly creates three different-colored pe
curveSpec.append(c)

#-----------------------------------------------------------------------------------------------------#
x = np.linspace(0, win_size, fs * win_size)
xT = np.linspace(0, win_size, fs * win_size)
ttemp = 1
time1 = 0
instant = 1
temporal_freq = []
aa = 0


def update():
    # Variables globales.

    global curve, y, x, xT, ri, r, EEG_n, counter, instant, headset, time, fs, win_size, board, y_p, knn, H, E, SD, freqs, ps, f, Y, y_p2, Ts, ttemp, inicio, time_final, time1, temporal_freq, aa

    EEG_new = get_n_secs(board, time)
    EEG_new = np.asarray(EEG_new)
    y2 = np.transpose(EEG_new.tolist())

    #--------------------------------------------------------#
    # Almacena EEG.
    with open("SavedData/raws2f2.txt", "a") as file:
        np.savetxt(file, (np.asarray(EEG_new)))

    H_new = np.zeros((bands, 1))
    E_new = np.zeros((bands, 1))
    SD_new = np.zeros((bands, 1))

    #--------------------------EEG Plot----------------------#
    if np.ceil(counter) < win_size:
        y = np.c_[y, y2]
        x = xT[:y.shape[1]]
        y_f = pre_process_plot(y)
        y_p = y_f

    else:
        y = np.c_[y, y2]
        y = y[:, int(time * fs):]
        y_f = pre_process_plot(y)
        y_p = np.c_[y_p, y_f[:, (y_f.shape[1] - int(time * fs)):y_f.shape[1]]]
        y_p = y_p[:, int(time * fs):]

        fq_names = ['delta', 'theta', 'alpha', 'beta', 'gamma']

        #H_new = np.zeros((bands, 1))
        #E_new = np.zeros((bands, 1))
        #SD_new = np.zeros((bands, 1))
        y_p2 = pre_process(y)
        e_yp = np.mean(np.sum(abs(y_p2 ** 2), axis=1))
        for freq in range(bands):
            b = np.load('Offline_vars/filters/b_' + fq_names[freq] + '.npy')
            a = np.load('Offline_vars/filters/a_' + fq_names[freq] + '.npy')
            Y0f = filtfilt(b, a, y_p2)

            # Feature extraction:
            Fi = getFeatures(Y0f)
            H_new[freq] = getEntropy(Y0f)
            E_new[freq] = getEnergy(Y0f, e_yp)
            SD_new[freq] = getSD(Y0f)

        H = np.hstack((H, H_new))[:, 1:]
        E = np.hstack((E, E_new))[:, 1:]
        SD = np.hstack((SD, SD_new))[:, 1:]

        y_p4 = pre_process_cca(y)
        y_p3 = np.zeros((channels_analisys, 875))
        y_p3[:, :] = y_p4[channels_analisys:, :]

        coef = np.zeros((1, len(f)))
        for fr in range(0, len(f)):
            Wx, Wy, r, flag_c = cca(np.asarray(y_p3), Y[:, :, fr])
            coef[0, fr] = np.amax(r)
        if flag_c:
            i, clas = np.unravel_index(coef.argmax(), coef.shape)

            # scr_light=["Left","Top","Right","Bottom"]
            # coef2=coef/np.sum(coef)
            # if (coef2[0,clas])>=0.3:
            #     print "-----------------------------"
            #     print "The class is: ",clas,"----",scr_light[clas]
            #     print "-----------------------------"
            # else:
            #     print "-----------------------------"
            #     print "The class cannot be determined"
            #     print "-----------------------------"

    for i in range(channels):
        curve[i].setData(x, y_p[i])
    counter = counter + time

    for i in range(bands):
        curveH[i].setData(range(1, points_number + 1), H[i, :])
        curveE[i].setData(range(1, points_number + 1), E[i, :])
        curveSD[i].setData(range(1, points_number + 1), SD[i, :])

    y_p5 = pre_process2(y)
    # print(y_p5[0].shape[0])
    ps, freqs = getSpectrum(y_p5[channel_analisys, :], Ts)

    if y_p5[0].shape[0] == 875:
        if ttemp == 1:
            inicio = tm.time()
            ttemp = 2
            final = inicio
            # subprocess('ssvep_stimuli_1.py')
            # subprocess('temp.py')
        else:
            final = tm.time()
            # if int(final-inicio) >= time_final:
                # disconnect_board(board)
        Frq_, Spc_ = getwelch(y_p5[channel_analisys, :], fs)
        # Maximo del espectro entre las frecuencias minima de estimulo - df y maxima de estimulo + df Hz
        Ap = np.max(Spc_[np.argmin(np.abs(Frq_ - Fre_analisys[0])):np.argmin(np.abs(Frq_ - Fre_analisys[1]))])
        Ftemp = Frq_[np.where(Spc_ == Ap)] # Frecuencia correspondiente al maximo del espectro entre las frecuencias minima de estimulo - df y maxima de estimulo + df Hz

        # if instant == 4:
        Max_fre_ = Ftemp[0]
            # Numer_dats = 1
        # Verifica Rango de Frecuencia 1
        # if stimulus_freq[0]-df < Max_fre_ < stimulus_freq[0]+df:
        #     tt2 = stimulus_freq[0]
        #     F_1 = 1
        #     F_2 = 0
        #     F_3 = 0
        #     F_4 = 0
        # # Verifica Rango de Frecuencia 2
        # elif stimulus_freq[1]-df < Max_fre_ < stimulus_freq[1]+df:
        #     tt2 = stimulus_freq[1]
        #     F_1 = 0
        #     F_2 = 1
        #     F_3 = 0
        #     F_4 = 0
        # # Verifica Rango de Frecuencia 3
        # elif stimulus_freq[2]-df < Max_fre_ < stimulus_freq[2]+df:
        #     tt2 = stimulus_freq[2]
        #     F_1 = 0
        #     F_2 = 0
        #     F_3 = 1
        #     F_4 = 0
        # # Verifica Rango de Frecuencia 4
        # elif stimulus_freq[3]-df < Max_fre_ < stimulus_freq[3]+df:
        #     tt2 = stimulus_freq[3]
        #     F_1 = 0
        #     F_2 = 0
        #     F_3 = 0
        #     F_4 = 1
        # else:
        #     tt2 = 'No class'
        F_1 = 0
        F_2 = 0
        F_3 = 0
        F_4 = 0

            # for i in list(range(len(stimulus_freq))):

            #     if stimulus_freq[i]-df < Max_fre_ < stimulus_freq[i]+df:
            #         tt2 = stimulus_freq[i]
            #     else:
            #         tt2 = 'No class'
        # else:
            # instant = instant + 1

        # curveSpec[0].setData(Frq_[np.argmin(np.abs(Frq_-Fre_analisys[0])):np.argmin(np.abs(Frq_-Fre_analisys[1]))],Spc_[np.argmin(np.abs(Frq_-Fre_analisys[0])):np.argmin(np.abs(Frq_-Fre_analisys[1]))]/Ap)
        curveSpec[0].setData(Frq_, Spc_ / Ap)
        if time1 != int(final - inicio):
            time1 = int(final - inicio)
            print int(final - inicio)

            if len(temporal_freq) != 10:
                temporal_freq.append(int(Max_fre_))
                aa += 1
            else:
                temporal_freq[0] = temporal_freq[1]
                temporal_freq[1] = temporal_freq[2]
                temporal_freq[2] = temporal_freq[3]
                temporal_freq[3] = temporal_freq[4]
                temporal_freq[4] = temporal_freq[5]
                temporal_freq[5] = temporal_freq[6]
                temporal_freq[6] = temporal_freq[7]
                temporal_freq[7] = temporal_freq[8]
                temporal_freq[8] = temporal_freq[9]
                temporal_freq[9] = int(Max_fre_)

        print temporal_freq
        if len(temporal_freq) == 10:
            tt3 = scipy.stats.mode(temporal_freq)
            #tt3 = mode(temporal_freq)
            print tt3
            #     print "---------------------------------------------------------------------------------------"
            #     print ['Frecuencia reconocida: M1 ',tt2, 'Frecuencia reconocida: M2 ',tt3, 'Frecuencia detectada: ',Ftemp]
            #     print "---------------------------------------------------------------------------------------"
            # else:
            #     print "---------------------------------------------------------------------------------------"
            #     print ['Frecuencia reconocida: M1 ',tt2, 'Frecuencia reconocida: M2 ',tt3, 'Frecuencia detectada: ',Ftemp]
            #     print "---------------------------------------------------------------------------------------"
            # modo inicial.
            # execute_for_some_time(F_1,F_2,F_3,F_4,E_new[0][0],H_new[0][0],E_new[1][0],H_new[1][0],E_new[2][0],H_new[2][0],E_new[3][0],H_new[3][0])

            if 9 <= tt3 <= 10:
                tt2 = stimulus_freq[0]
                F_1 = 1
                F_2 = 0
                F_3 = 0
                F_4 = 0
            # Verifica Rango de Frecuencia 2
            elif 13 <= tt3 <= 14:
                tt2 = stimulus_freq[1]
                F_1 = 0
                F_2 = 1
                F_3 = 0
                F_4 = 0
            # Verifica Rango de Frecuencia 3
            elif 6 <= tt3 <= 8:
                tt2 = stimulus_freq[2]
                F_1 = 0
                F_2 = 0
                F_3 = 1
                F_4 = 0
            # Verifica Rango de Frecuencia 4
            elif 22 <= tt3:
                tt2 = stimulus_freq[3]
                F_1 = 0
                F_2 = 0
                F_3 = 0
                F_4 = 1
            else:
                tt2 = 'No class'
                F_1 = 0
                F_2 = 0
                F_3 = 0
                F_4 = 0

            print(F_1, F_2, F_3, F_4)

        execute_for_some_time(F_1, F_2, F_3, F_4, E_new[0][0], H_new[0][0], E_new[1][0], H_new[1][0], E_new[2][0], H_new[2][0], E_new[3][0], H_new[3][0])


timer = QtCore.QTimer()
timer.timeout.connect(update)
timer.start(0)

if __name__ == '__main__':
    if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'):
        QtGui.QApplication.instance().exec_()
    disconnect_board(board)
